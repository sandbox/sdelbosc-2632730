INTRODUCTION
------------

The Search API Solr Administration module extends the Search API Solr module by
exposing debug information to the administrator.

REQUIREMENTS
------------

The module requires Search API Solr to be installed.

Optional: the module also extends the Search API Acquia module, but does not
depend on it.

CONFIGURATION
-------------

In order to access the debug information the
`view search api solr admin debug info` permission should be granted to the
trusted role whose users should access debug information.

Once the role is set up, debugging can be configured on the Solr service
configuration page.

FEATURES
--------

 * Debug options on Search API server configuration tab to display Solr queries
   and responses as Drupal messages.
 * Adds a new "Request" tab on Search API server administration that allows to
   type a request and send it to Solr. Example: admin/luke. This is helpful on
   environments where Solr administration is not accessible directly.
 * Drush integration: the `sapisa-c` command is available which hard flushes
   the all the indices used by all configured Solr cores for the site. This is
   useful when the content on the site becomes disconnected from the contents
   of the search index (e.g. from a previous installation).
