<?php

namespace Drupal\search_api_solr_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\ServerInterface;

/**
 * A basic form with a passed entity with an interface.
 */
class SolrRequestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'solr_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ServerInterface $search_api_server = NULL) {
    $form['#title'] = $this->t('Send requests to Solr');

    $form['server'] = array(
      '#type' => 'value',
      '#value' => $search_api_server,
    );

    $form['request'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Request'),
      '#required' => TRUE,
      '#description' => $this->t('The request to send to Solr, ex: admin/luke.'),
      '#size' => 50,
      '#maxlength' => 500,
    );

    $response = $form_state->getValue('response') ?: '';
    $form['response'] = array(
      '#markup' => '<div id="response">' . $response . '</div>',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    try {
      $response = search_api_solr_admin_server_send_request($form_state->getValue('server'), $form_state->getValue('request'));
      $response = '<pre>' . print_r($response, TRUE) . '</pre>';
    }
    catch (SearchApiException $e) {
      watchdog_exception(
        'search_api_solr_admin', $e,
        '%type while sending request to Solr server @server: !message in %function (line %line of %file).',
        array('@server' => $form_state->getValue('server')->label())
      );
      $response = $this->t('An error occurred while sending the request to Solr.');
    }

    $form_state->setValue('response', $response);
  }

}
