<?php

/**
 * @file
 * Drush commands for Search API Solr Administration.
 */

use Drupal\search_api\Entity\Server;

/**
 * Implements hook_drush_command().
 */
function search_api_solr_admin_drush_command() {
  $items = array();

  $items['search-api-solr-admin-clear'] = array(
    'description' => 'Hard clear of Solr servers. Useful after a site reinstallation where stale data remain indexed. Think twice.',
    'aliases' => array('sapisa-c'),
  );

  return $items;
}

/**
 * Clear whole index.
 */
function drush_search_api_solr_admin_clear() {
  // Clear anything related to current site.
  $indexes = search_api_drush_get_indexes();
  if (empty($indexes)) {
    return;
  }
  foreach ($indexes as $index) {
    $index->clear();
    drush_log(dt('@index was successfully cleared.', array('@index' => $index->label())), 'ok');
  }

  // Clear whatever remains.
  $servers = Server::loadMultiple();
  if (empty($servers)) {
    return;
  }
  foreach ($servers as $server) {
    if (
      $server->getBackendId() == 'search_api_solr'
    ) {
      $server->getBackend()->deleteAllIndexItems();
      drush_log(dt('@server was successfully cleared.', array('@server' => $server->label())), 'ok');
    }
  }
}
