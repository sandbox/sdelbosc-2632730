<?php

/**
 * @file
 * Contains SearchApiAcquiaSearchServiceSASAS.
 */

if (!class_exists('SearchApiAcquiaSearchService')) {
  return;
}

/**
 * Acquia Search service class for Search API Solr Admin.
 */
class SearchApiAcquiaSearchServiceSASAS extends SearchApiAcquiaSearchService {

  /**
   * Overrides SearchApiAcquiaSearchService::configurationForm().
   */
  public function configurationForm(array $form, array &$form_state) {
    $form = parent::configurationForm($form, $form_state);
    return SearchApiSolrServiceSASAS::customiseConfigurationForm($this->options, $form);
  }

  /**
   * Overrides SearchApiAcquiaSearchService::preQuery().
   */
  protected function preQuery(array &$call_args, SearchApiQueryInterface $query) {
    parent::preQuery($call_args, $query);
    SearchApiSolrServiceSASAS::customisePreQuery($this->options, $call_args);
  }

  /**
   * Overrides SearchApiAcquiaSearchService::postQuery().
   */
  protected function postQuery(array &$results, SearchApiQueryInterface $query, $response) {
    parent::postQuery($results, $query, $response);
    SearchApiSolrServiceSASAS::customisePostQuery($this->options, $response);
  }

}
