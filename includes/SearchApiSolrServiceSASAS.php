<?php

/**
 * @file
 * Contains SearchApiSolrServiceSASAS.
 */

/**
 * Search API service class for Search API Solr Admin.
 */
class SearchApiSolrServiceSASAS extends SearchApiSolrService {

  /**
   * Overrides SearchApiSolrService::configurationForm().
   */
  public function configurationForm(array $form, array &$form_state) {
    $form = parent::configurationForm($form, $form_state);
    return SearchApiSolrServiceSASAS::customiseConfigurationForm($this->options, $form);
  }

  /**
   * Overrides SearchApiSolrService::preQuery().
   */
  protected function preQuery(array &$call_args, SearchApiQueryInterface $query) {
    parent::preQuery($call_args, $query);
    SearchApiSolrServiceSASAS::customisePreQuery($this->options, $call_args);
  }

  /**
   * Overrides SearchApiSolrService::postQuery().
   */
  protected function postQuery(array &$results, SearchApiQueryInterface $query, $response) {
    parent::postQuery($results, $query, $response);
    SearchApiSolrServiceSASAS::customisePostQuery($this->options, $response);
  }

  /**
   * Utility function for config form update.
   */
  public static function customiseConfigurationForm($options, $form) {
    $options = $options += array(
      'debug_query' => FALSE,
      'debug_response' => FALSE,
    );

    $form['advanced']['debug_query'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug search requests'),
      '#description' => t('Display all outgoing Solr search requests.'),
      '#default_value' => $options['debug_query'],
    );
    $form['advanced']['debug_response'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug search results'),
      '#description' => t('Display all search result responses received from Solr.'),
      '#default_value' => $options['debug_response'],
    );

    return $form;
  }

  /**
   * Utility function for pre query customisations.
   */
  public static function customisePreQuery($options, &$call_args) {
    // Debug response?
    if (
      !empty($options['debug_response'])
      && user_access('view search api solr admin debug info')
    ) {
      $call_args['params']['debugQuery'] = 'on';
    }

    // Debug query?
    if (
      !empty($options['debug_query'])
      && user_access('view search api solr admin debug info')
    ) {
      drupal_set_message(t(
        'Solr query: <pre>@args</pre>',
         array(
           '@args' => print_r($call_args, TRUE),
         )
      ));
    }
  }

  /**
   * Utility function for post query customisations.
   */
  public static function customisePostQuery($options, $response) {
    // Debug response?
    if (
      !empty($options['debug_response'])
      && user_access('view search api solr admin debug info')
    ) {
      drupal_set_message(
        t('Solr response: <pre>@response<pre>', array(
          '@response' => print_r($response, TRUE),
        ))
      );
    }
  }

}
